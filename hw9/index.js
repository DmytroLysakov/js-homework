// Теоритичні питання
// Завдання №1
// Створити новий HTML тег на сторінці можна використовуючи метод document.createElement(tag), та метод document.createTextNode('А ось і я'), перший являється загально прийнятим.
// Завдання №2
// Перший параметр функції insertAdjacentHTML - означає позицію відносно елемента до якого використано метод.
// Можнга вставити елемент перед beforeBegin відносно опорного елемента, в після afterEnd опроного елемента,
// а також впочаток самого опрного елемента afterBegin та в кінець опорного елемента  beforeEnd.
// Завдання №3
// Щоб видалити елемент із сторінки можна скористатися методом .remove(). Або методом replace() який знайде необхідний елемент видалить його та замінити на новий вказаний.
// Практичне завдання.


function showListPage(arr, parent = document.body) {
    if(!Array.isArray(arr)){
        return parent.insertAdjacentHTML('beforeend', `<li>${arr}</li>`);
    }
    else {
        let ul = document.createElement('ul');
        let newParent = parent.appendChild(ul);
        return arr.map((elem) => {
            return showListPage(elem, newParent)
        })
    }
}
firstArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
secondArr = ["1", "2", "3", "sea", "user", 23];
thirdArr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

showListPage(firstArr)
showListPage(secondArr)
showListPage(thirdArr)

let div = document.createElement('div');
div.className = "timer";
div.innerHTML = '';
document.body.prepend(div)

let i = 3;
let timer = div
timer.innerHTML = `Сторінка очиститься через ${i} секунд`;
let innerTimer = setInterval(function(){
    i--;
    if (i === 0){
        document.body.innerHTML = "";
        clearTimeout(innerTimer);
    }
    else {
        timer.innerHTML = `Сторінка очиститься через ${i} секунд`
    }
},1000);
