// Теоретичні завдання
// Питання №1
// Методи об'єкта це функція яка належить об'єкту, ключовим в якій є конструктор Object.
// Питання №2
// Значеня властвості об'єкта може мати будь який тип даних, як-от рядки, числа, логічні значення,
// або складні типи даних, функції та інші об’єкти.
// Питання №3
// Об'єкт це посилальний тип даних. Що означає це поняття?
// Це означає що об'єкт копіюється за посиланням, а не зазначеннями як примітивні значення. При перевіркаха буде перевірятися саме посилання.


 let createNewUser = function (){
     let _firstName = prompt(`Please enter your first name`)
        while (!_firstName || !isNaN(_firstName) || _firstName === null || _firstName.search(/\d/) != -1 || _firstName.search(/[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/) != -1) {
                _firstName = prompt(`Your name was entered incorrectly, please try again`)
     }
     let _lastName = prompt(`Please enter your last name`)
        while (!_lastName || !isNaN(_lastName) || _lastName === null || _lastName.search(/\d/) != -1 || _lastName.search(/[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/) != -1) {
                _lastName = prompt(`Your last name was entered incorrectly, please try again`)
     }
     return {
         firstName: _firstName,
         lastName: _lastName,
    }
}
let myUser = createNewUser();

 Object.defineProperty(myUser,
     'getLogin', {get: function() {
         return this.firstName[0]+this.lastName.toLowerCase();
     }
 })

 console.log(myUser)


