const student = {
	name: '',
	lastName: '',
	tabel: {},
};

function getText(text) {
	return prompt(text);
}

function setTableValues(course, rating) {
	student.tabel[course] = rating;
}

function getBadPoint() {
	let count;
	for (let key in student.tabel) {
		//console.log(key, student.tabel[key]);
		if (student.tabel[key] < 4) {
			count++;
		}
		else {
			count = 0;
		}
	}
	if (count === 0) {
		console.log('Студент переведен на следующий курс');
	}
}

function getGpa() {
	let arrTableValue = Object.values(student.tabel);
	let sum = 0;
	for (let i = 0; i < arrTableValue.length; i++) {
		sum += arrTableValue[i];
	}
	if ((sum / arrTableValue.length) > 7) {
		console.log(`gpa: ${Math.round((sum / arrTableValue.length) * 100) / 100} - Студенту назначена стипендия`);
	}
	//console.log(`gpa:${sum / arrTableValue.length}`);
}

function setTable() {
	do {
		let nameCourse = getText('Enter name of course');
		if (nameCourse === null) {
			break;
		}
		let ratingCourse = getText(`Enter your rating of - "${nameCourse}"`);
		// function validation nameCourse and ratingCourse, need ???
		nameCourse = nameCourse.trim();
		ratingCourse = Number(ratingCourse);
		setTableValues(nameCourse, ratingCourse);
	} while (true);
}

student.name = getText('Enter your Name');
student.lastName = getText('Enter your LastName');
setTable();
getBadPoint();
getGpa();
console.log(student);
