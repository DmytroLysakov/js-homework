const passwordForm = document.querySelector('.password-form');
passwordForm.addEventListener('click', changeType);
passwordForm.addEventListener('submit', comparePassword);
const basicPass = document.getElementById('basic-pass');
const checkPass = document.getElementById('check-pass');


function changeType(event) {
   if (event.target.closest('.fas')) {
      event.target.classList.toggle('fa-eye');
      event.target.classList.toggle('fa-eye-slash');
      if (event.target.classList.contains('fa-eye')) {
         event.target.parentNode.getElementsByTagName('input')[0].setAttribute('type', 'text');
      } else {
         event.target.parentNode.getElementsByTagName('input')[0].setAttribute('type', 'password');
      }
   }
}


function comparePassword(event) {
   event.preventDefault();
   const basicPassValue = basicPass.value;
   const checkPassValue = checkPass.value;

   if (document.getElementById('err')) {
      document.getElementById('err').remove()
   }

   if (!basicPassValue && !checkPassValue) {
      checkPass.insertAdjacentHTML('afterend', `<p id='err' style="color:red;">Потрібно ввести значення</p>`);
   } else if (basicPassValue === checkPassValue) {
      alert('You are welcome')

   } else {
      checkPass.insertAdjacentHTML('afterend', `<p id='err' style="color:red;">Потрібно ввести однакові значення</p>`);
   }
}

