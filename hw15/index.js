// Теоричине завдання
// Рекурсія це приймо коли функція викликає сама себу.

let userNumber = prompt('Please enter a your number')
while (isNaN(userNumber) || userNumber === null || parseInt(userNumber) !== +userNumber  || userNumber < 1){
    userNumber = prompt ("Please enter your number (the number must be: integer,positive)").trim()
}

function  factorial (n) {
    return (n > 0 && n <= 1) ? n : (n * factorial(n - 1));
}


alert(`Factorial of your number is ${factorial(userNumber)}`)