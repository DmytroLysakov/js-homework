//Теоретичні завдання.
// Завданя №1
// Метод forEach дає можливість почергово перебрати всі елементи масиву. Метод forEach в параметр отримує функцію
// яка виконується для кожного елемента в масиві. Функція прийме параметри типу arr.user, index, item і.т.д.
// Завдання №2
// Масив можна очистити за допомогою медота length. Задавши значення arr.length = [0]; довжина масиву стане 0 він стане пустиим. Слід памятати що ця дія безворотння.
// Завдання №3
//За допомогою метода typeof

let myArr = [`hello`, 1, `world`, 2,
   { "first": 1, 2: "second" },
   [3, `for`, 5, `six`],
   null, undefined, false, true,];

function filterBy(arr, type) {
   let insideArr = arr.filter(function(element){
      return typeof element !== type;
   })
   return insideArr;
}

const allTypes = [`string`, `number`, `boolean`,`object`];
allTypes.forEach(type => console.log(filterBy(myArr, type)));