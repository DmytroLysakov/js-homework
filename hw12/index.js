// Теоритичне завдання
// Це не надійно тому, що ввід даних не обов'язково виконується із допомогою фізичної клавіатури з обробниками подій.

let btnWrap = document.querySelector('.btn-wrapper');
document.addEventListener('keydown', changeColor);

function changeColor(event) {
   Array.from(btnWrap.children).forEach(btn => btn.dataset.keycode === event.code ? btn.classList.add('blue') : btn.classList.remove('blue'));
}

