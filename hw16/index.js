let f0 = 1
let f1 = 2
let userNum = +prompt('Введіть число n')
while ((isNaN(userNum)) || (!userNum) || (userNum > 30) || (userNum < -30)) {
   userNum = +prompt("Введіть число(n)", `${userNum}`)
}

function fib(f0, f1, n) {
   if (n === 0) {
      return f0;
   } else if (n === 1) {
      return f1;
   } else if (n > 1) {
      return fib(f0, f1, n - 1) + fib(f0, f1, n - 2);
   } else if (n < 0) {
      return fib(f0, f1, n + 2) - fib(f0, f1, n + 1);
   }
}

alert(fib(f0, f1, userNum));