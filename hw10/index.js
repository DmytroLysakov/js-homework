// initialization class tab
const tabs = document.querySelectorAll('.tab');
// initialize class content
const contents = document.querySelectorAll('.content');
// add event on each tab
for( let i = 0; i < tabs.length; i++ ){
    tabs[i].addEventListener('click',(event) => {
        // delete class in tab
        let tabsCurrent = event.target.parentElement.children;
        for( let j = 0; j < tabsCurrent.length; j++ ){
            tabsCurrent[j].classList.remove('active')
        }
        // add class in tab
        event.target.classList.add('active');

        // delete class in content
        let contentsCurrent = event.target.parentElement.nextElementSibling.children;
        for( let t = 0; t < contentsCurrent.length; t++ ){
            contentsCurrent[t].classList.remove('active');
        }
        // add class in content
        contents[i].classList.add('active');
    })
}
