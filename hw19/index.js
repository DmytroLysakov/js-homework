const speedTeam = [2, 2, 2, 2,];
const listTasks = [8, 8, 8];
let dateReleaseTxt = '01-03-2023';

const dateRelease = new Date(dateReleaseTxt);
let complete = false;
let result = '';

function getComlete(arrSpeedTeam, arrListTasks, date) {

	let sumTeamPoint = sumPoint(arrSpeedTeam);
	let sumTasksPoint = sumPoint(arrListTasks);

	let speedTeam_hour = sumTeamPoint / 8; // point per hour
	let fullDayToRelease = dateRelease.getDate() - new Date().getDate();

	// how work day
	let selectDay = new Date();
	let workDayToRelease = fullDayToRelease;
	for (let i = 1; i <= fullDayToRelease; i++) {
		if (selectDay.getDay() === 0 || selectDay.getDay() === 6) {
			workDayToRelease--;
		}
		selectDay.setDate(selectDay.getDate() + 1);
	}

	// result
	let timeToComlete = Math.round(sumTasksPoint / speedTeam_hour) - workDayToRelease * 8;
	if (timeToComlete > 0) {
		complete = false;
		result = `Команді розробників доведеться витратити додатково ${parseInt(timeToComlete / 8)} днів та ${Math.round(timeToComlete) % 8} годин після дедлайну, щоб виконати всі завдання в беклозі`;
	}
	else {
		complete = true;
		result = `Усі завдання будуть успішно виконані за ${Math.abs(parseInt(timeToComlete / 8))} днів і ${Math.abs(Math.round(timeToComlete) % 8)} днів до настання дедлайну!`;
	}

	return result;
}

function sumPoint(arr) {
	return arr.reduce((val, item) => (val + item), 0);
}



//********************************************
console.log(getComlete(speedTeam, listTasks, dateRelease))
alert(getComlete(speedTeam, listTasks, dateRelease))
