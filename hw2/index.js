// Теоретичні завдання
// Типи даних у JS:
// Number (числовий) => let a = 1;
// String (рядок)   => let a = "some string";
// Boolean (логічний) => let a = true or false;
// Object (об'єкт) => let a = ["apple", "orange","banana"](масив)- колекції даних або слкадні дані;
// Symbol (символ) => const s1 = Symbol() - використовується для створення унікальних індетифікаторів об'єктів;
// Null - тип для невідомих значень що має лише значення null;
// Undefined - тип для не присвоєних значень що має лише значення undefined (var a =; console.log(a);- undefined;

// Різниця між == та === - у рівності це різні
// == - оператор не строгого порівння;
// === - оператор строгого порівння;

// Оператори це - внутрішні вункції мови програмування, якими задаються певні кроки обробки інформації.
// В JS розрызняють такі види операторів:
// Оператори присвоєння;
// Арифметичні оператори;
// Оператори порівння;
// Логічні оператори;


// Практичне завдання

let userName = prompt("Please enter your name", "");
while (!userName || !isNaN(userName) || userName === null) {
    userName = prompt("Your name was entered incorrectly, please try again", "");
}
console.log(userName);
let userAge = prompt("Please enter your age", "");
while (!userAge || isNaN(userAge) || userAge === null || userAge <= 0) {
    userAge = prompt("Your age was entered incorrectly, please try again");
}
console.log(userAge);
if (userAge < 18) {
    alert("You are not allowed to visit this website")
}else if (userAge > 22) {
    alert(`Welcome ${userName}`)
} else if (userAge <= 22 && confirm("Are you sure you want to continue?")) {
    alert (`Welcome ${userName}`)
} else  {
    alert("You are not allowed to visit this website")
}


