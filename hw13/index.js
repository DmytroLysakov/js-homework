document.querySelector('.images-wrapper').insertAdjacentHTML("afterend", `
    <div class="buttons">
        <button id="stop">Припинити</button>
        <button id="play">Відновити показ</button>
    </div>
    <div class="timer"></div>`)

const timerDiv = document.querySelector('.timer');
let isStopped = false;
let carousel;
let count;
let timerInterval

function startTimer() {
    const startTime = Date.now();

    timerInterval = setInterval(function () {
        timerDiv.innerHTML = `До зміни слайду ${Math.abs(((Date.now() - startTime) / 1000) -3).toFixed(3)} секунд`;
    }, 10);
}

function changeImg(i){
    const images = document.querySelectorAll('.image-to-show');

    count = i;
    startTimer();
    if (images[i] && images[i].classList.contains('fadeOut')){
        images[i].classList.remove('fadeOut');
    }
    images[i].classList.add("active");
    carousel = setTimeout(() =>{
        images[i].classList.add('fadeOut');

        setTimeout(() => {
            images[i].classList.remove('active');
            i++;
            count = i;
            if (i > images.length - 1) {
                i = 0;
            }
            clearInterval(timerInterval);
            changeImg(i);

        },500)
    }, 2500);
}

changeImg(0);

document.querySelector("#stop").addEventListener("click", e => {
    if (e.target.closest("#stop")) {
        clearTimeout(carousel);
        clearInterval(timerInterval);
        isStopped = true;
        timerDiv.innerHTML = "Анімація запущена";
    }
})

document.querySelector("#play").addEventListener("click", e => {
    if (e.target.closest("#play") && isStopped){
        isStopped = false;
        changeImg(count)
    }
})