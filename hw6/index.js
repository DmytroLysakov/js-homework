// Теоретичні завдання
// Питання №1
// Екранування це один із спеціальних символів в регуляних виразах. В програмувані позачається як слеш\
// використовуєтья для символів які зазвичай опрацьовуються особливим способом а також для пошуку таких символів $ @ []
// Питання №2
// Функцыональний вираз (Function declaration), звичайна функція (Function expression),анонімна функція, стрілкова фунція,
// Питання №3
// Hosting - це процес за допомогою якого інтерпретатор переміщує оголошення функції,зміних у верхню частину їхньої області.

function createNewUser() {

    let newUser = {

        setFirstName(newFirstName) {
            Object.defineProperty(this, 'firstName', {
                value: newFirstName,
            });
        },
        setLastName(newLastName) {
            Object.defineProperty(this, 'lastName', {
                value: newLastName,
            });
        },

    };

    Object.defineProperty(newUser, 'firstName', {
        value: prompt(`Please enter your first name`),
        writable: false,
        configurable: true,
    });
    Object.defineProperty(newUser, 'lastName', {
        value: prompt(`Please enter your last name`),
        writable: false,
        configurable: true,
    });
    Object.defineProperty(newUser, 'getLogin', {
        get: function() {
         return this.firstName[0]+this.lastName.toLowerCase();},
    });
    Object.defineProperty(newUser, 'birthday', {
        value: prompt(`Please enter your birth date`,("dd.mm.yyyy")),
        writable: false,
        configurable: true,
    });
    Object.defineProperty(newUser, 'getPassword', {
        get: function (){
            return (this.firstName[0]).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice([6])
        }
    })
    Object.defineProperty(newUser, 'getAge', {
        get: function (){
            let age = new Date(this.birthday)
            return age
        }
    })
    // newUser.firstName('Test1')
    // newUser.lastName('Test2')
    // newUser.setFirstName("Test2")
    // newUser.setLastName("Test1")
    return newUser;
}

let newUser = createNewUser();
console.log(newUser)
console.log(newUser.getLogin)
console.log(newUser.birthday)
console.log(newUser.getPassword)
console.log(newUser.getAge)