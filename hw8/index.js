// Теоритичні завжання
// Завдання №1
// Document Object Model (DOM) - об'єктна модель документа. Модель документа DOM формуэться із завантаженого браузером HTML коду.
// Браузери створючи DOM дають можливість швидко маніпулювати веб-документом:шукати , додавати, заміняти, отримувати необхідні елементи.
// DOM за свлэю строкутурою дуже сходий на HTML код, але не являєтья ним - лишегь формується з нього. Має деревоподібну ієархію, формується з
// Вузлів Node, які в свою чергу можуть містити вбудовані вузли, елементи, текст, коментарі. Кожен вузол DOM формуєтьяс з тегу HTML та отримує,
// його властивості, атрибути, події, стилі.
// Завдання №2
//  innerHTML - при воді видасть всю інформацію заключену в елементі від початку до закриття тегу досівно, в тому числі і розмітку HTML (всі вказані в цьому елементі HTML теги).
//  innerText - при воды видасть інформацію заключену в елементі такою як її передає браузер для користувача, розмітка HTML також буде взято до уваги, про те буде відображено тількі візуално, без дослівного відображеня тегів HTML елемента.
// Завдання №3
// За ідентифікатором - ID (id=myName);
// За класом - елементи з класом (class = item.price;)
// За тегом - елементи з тегом (div, li, p;)
// За CSS селектором чи селекторами querySelector і querySelectorAll, які і являються найбліш популярними та часто задіяними в новому коді;

// Практичне завдання
let allParagraphs = document.querySelectorAll("p");
for (let i = 0; i < allParagraphs.length; i++) {
  allParagraphs[i].style.backgroundColor = "#ff0000";
}


let optionsList = document.getElementById('optionsList');
console.log(optionsList)
console.log(optionsList.parentElement);

for (let child of optionsList.childNodes) {
   console.log(child.nodeType, child.nodeName);
}

// let optionsList = document.getElementById('optionsList');
// console.log(optionsList)
// let optionsListParent = optionsList.parentElement
// for (let i = 0; i < optionsListParent.childNodes.length; i++) {
//       console.log( optionsListParent.childNodes[i] );
//     }
//
// let optionsListChild = optionsList.children


let p = document.getElementById('testParagraph').innerHTML = 'This is a paragraph';

// let mainHeaderContent = document.querySelector('.main-header')
// console.log(mainHeaderContent)
// let elementsInMainHeader = mainHeaderContent.children
// console.log(elementsInMainHeader)
// let firstElementInMainContent = mainHeaderContent.children[0]
// firstElementInMainContent.classList.replace('main-header-content','nav-item')
// let secondElementInMainContent = mainHeaderContent.children[1]
// secondElementInMainContent.classList.replace('d-flex','nav-item')
// console.log(elementsInMainHeader)

let mainHeaderContent = document.querySelector('.main-header')
for (let elem of mainHeaderContent.children) {
   console.log(elem);
   elem.classList.add("nav-item");
}

// document.querySelector('.section-title').remove();
let allElementsWithClass = document.querySelector('.section-title')
allElementsWithClass.classList.remove('section-title')
console.log(allElementsWithClass)